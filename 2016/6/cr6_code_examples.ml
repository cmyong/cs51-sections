(*** CrawlerServices (the artist formerly known as Util ***)
(**********************************************************)
module type CS =
  sig
    type link = { host : string ;  (* e.g., "www.eecs.harvard.edu" *)
		  port : int ;     (* e.g., 80 *)
		  path : string    (* e.g., "/~greg/index.html" *)
		}
		  
    val string_of_link : link -> string
    val href_of_link: link -> string
    val link_compare : link -> link -> ordering
					 
    (* pages are used to describe the contents of web pages *)
    type page = { url : link ;          (* see above *)
		  links : link list ;   (* all of the links on the page *)
		  words : string list   (* all of the words on the page *)
		}
    val string_of_page : page -> string
    (* given the link, returns the page -- should specify what
     * exceptions get raised. *)
    val get_page : link -> page option
				
    (* the initial link to be used by the crawler *)
    val initial_link : link
    (* The root directory of the server. "" if crawling the web,
    (dirname initial_link) otherwise *)
    val root_dir : string
    (* the number of (distinct) pages the crawler should process *)
    val num_pages_to_search : int
    (* the port on which to listen for query requests *)
    val server_port : int
  end
    
(*** DICT ***)
(************)
module type DICT =
  sig
    type key
    type value
    type dict

    (* An empty dictionary *)
    val empty : dict

    (* Reduce the dictionary using the provided function f and base case u.
     * Our reducing function f must have the type:
     *      key -> value -> 'a -> 'a
     * and our base case u has type 'a.
     *
     * If our dictionary is the (key,value) pairs (in any order)
     *      (k1,v1), (k2,v2), (k3,v3), ... (kn,vn)
     * then fold should return:
     *      f k1 v1 (f k2 v2 (f k3 v3 (f ... (f kn vn u))))
     *)
    val fold : (key -> value -> 'a -> 'a) -> 'a -> dict -> 'a

    (* Returns as an option the value associated with the provided key. If
     * the key is not in the dictionary, return None. *)
    val lookup : dict -> key -> value option

    (* Returns true if and only if the key is in the dictionary. *)
    val member : dict -> key -> bool

    (* Inserts a (key,value) pair into our dictionary. If the key is already
     * in our dictionary, update the key to have the new value. *)
    val insert : dict -> key -> value -> dict

    (* Removes the given key from the dictionary. If the key is not present,
     * return the original dictionary. *)
    val remove : dict -> key -> dict

    (* Return an arbitrary key, value pair along with a new dict with that
     * pair removed. Return None if the input dict is empty *)
    val choose : dict -> (key * value * dict) option

    (* functions to convert our types to strings for debugging and logging *)
    val string_of_key: key -> string
    val string_of_value : value -> string
    val string_of_dict : dict -> string

    (* various testing functions! *)
    val test_balance : unit -> bool
    val test_insert : unit -> bool
    val test_remove : unit -> bool
    val test_choose : unit -> bool
    val test_fold : unit -> bool

    (* Runs all the tests. see TESTING EXPLANATION below *)
    val run_tests : unit -> bool
  end

(*** DICT_ARG ***)
(****************)
module type DICT_ARG =
  sig
    type key
    type value
    val compare : key -> key -> ordering
    val string_of_key : key -> string
    val string_of_value : value -> string

    (* Use these functions for testing. See TESTING EXPLANATION. *)

    (* Generate a key. The same key is always returned *)
    val gen_key : unit -> key

    (* Generate a random key. *)
    val gen_key_random : unit -> key

    (* Generates a key greater than the argument. *)
    val gen_key_gt : key -> unit -> key

    (* Generates a key less than the argument. *)
    val gen_key_lt : key -> unit -> key

    (* Generates a key between the two arguments. Return None if no such
     * key exists. *)
    val gen_key_between : key -> key -> unit -> key option

    (* Generates a random value. *)
    val gen_value : unit -> value

    (* Generates a random (key,value) pair *)
    val gen_pair : unit -> key * value
  end

(*** SET ***)
(***********)
module type SET =
  sig
    type elt  (* type of elements in the set *)
    type set  (* abstract type for the set *)

    val empty : set

    val is_empty : set -> bool

    val insert : elt -> set -> set

    (* same as insert x empty *)
    val singleton : elt -> set

    val union : set -> set -> set
    val intersect : set -> set -> set

    (* remove an element from the set -- if the
     * element isn't present, does nothing. *)
    val remove : elt -> set -> set

    (* returns true iff the element is in the set *)
    val member : set -> elt -> bool

    (* chooses some member from the set, removes it
     * and returns that element plus the new set.
     * If the set is empty, returns None. *)
    val choose : set -> (elt * set) option

    (* fold a function across the elements of the set
     * in some unspecified order. *)
    val fold : (elt -> 'a -> 'a) -> 'a -> set -> 'a

    (* functions to convert our types to a string. useful for debugging. *)
    val string_of_set : set -> string
    val string_of_elt : elt -> string

    (* runs our tests. See TESTING EXPLANATION *)
    val run_tests : unit -> unit
  end

(*** COMPARABLE (SET_ARG) ***)
(****************************)
module type COMPARABLE =
  sig
    type t
    val compare : t -> t -> ordering
    val string_of_t : t -> string

    (* The functions below are used for testing. See TESTING
     EXPLANATION *)

    (* Generate a value of type t. The same t is always returned *)
    val gen : unit -> t

    (* Generate a random value of type t. *)
    val gen_random : unit -> t

    (* Generate a t greater than the argument. *)
    val gen_gt : t -> unit -> t

    (* Generate a t less than the argument. *)
    val gen_lt : t -> unit -> t

    (* Generate a t between the two arguments. Return None if no such
     * t exists. *)
    val gen_between : t -> t -> unit -> t option
  end
