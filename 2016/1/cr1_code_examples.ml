(*
 * ......................................................................
 * Lab 1 Exercise 8: Define a function that returns the maximum element in an
 * integer list, or None if there are no elements in the list.
 * ......................................................................
 *)

let rec max_list (lst : int list) : int option =
  match lst with
  | [] -> None
  | hd :: tl ->
	 match max_list tl with
	 | None -> Some hd
	 | Some mx -> Some (max hd mx)
(*   | Some mx -> if hd > mx then Some hd else Some mx *) 
(*   | Some mx -> Some (if hd > mx then hd else mx) *)
(*   | Some mx as some_mx -> if hd > mx then Some hd else some_mx *)
(** These are just a few different ways you could have implmemented this last line.
 *  I'm a fan of the first one, thats uncommented, since it's simple. The last one
 *  is mostly there as an example of how you can use the `as` keyword 
 *)
					   
(*
 * ......................................................................
 * Lab 1 Exercise 9: Define a function zip, that takes two int lists and
 * returns a list of pairs of ints, one from each of the two argument
 * lists. Make sure that the function handles cases of mismatched list
 * lengths by returning None in that case. For example, zip [1;2;3]
 * [4;5;6] should evaluate to Some [(1, 4); (2, 5); (3, 6)].
 * 
 * Why wouldn't it be possible, in cases of mismatched length lists, to
 * just pad the shorter list with, say, false values, so that,
 * zip [1] [2; 3; 4] = [(1, 2); (false, 3); (false, 4)]?
 *  ......................................................................  
 *)

let rec zip (x : int list) (y : int list) : ((int * int) list) option =
  match x, y with
  | [], [] -> Some []
  | hdx :: tlx, hdy :: tly ->
	 (match zip tlx tly with
	  | None -> None
	  | Some lst -> Some ((hdx, hdy) :: lst))
  | _, _ -> None

(** The top implementation uses the _ wildcards effectively, but it requires
 *  nesting a match and using parens, where the second puts the nested match
 *  on the end, where it doesn't need to be wrapped in parens *)
	   
(*
 * ......................................................................
 * Lab 1 Exercise 13: The OCaml List module provides, in addition to the map,
 * fold_left, and fold_right higher-order functions, several other useful
 * higher-order list manipulation functions. For instance, map2 is like
 * map, but takes two lists instead of one along with a function of two
 * arguments and applies the function to corresponding elements of the
 * two lists to form the result list. Use map2 to reimplement zip.
 * ......................................................................
 *)

let zip_ho (x : int list) (y : int list) : ((int * int) list) option =
  if List.length x == List.length y then
	Some (List.map2 (fun xhd yhd -> (xhd, yhd)) x y)
  else
	None

(** This is just a nice example of how useful higher order functions are,
 *  for you to keep in mind as you do ps1. Some things can be much easier
 *  to read and maintain if they're written using map, fold, etc. *)
